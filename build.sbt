ThisBuild / version          := "0.1.0-SNAPSHOT"

lazy val commonSettings = Seq(
  scalaVersion     := "2.13.7",
  organization     := "com.example",
  organizationName := "example",
  name := "monix-example",
  scalafmtOnCompile in ThisBuild := true,
  Test / fork := true,
  libraryDependencies ++= Dependencies.common ++ Dependencies.test
)


lazy val root = (project in file("."))
  .settings(commonSettings)
