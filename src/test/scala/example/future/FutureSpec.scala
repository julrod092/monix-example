package example.future

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.funsuite.AsyncFunSuiteLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class FutureSpec extends AsyncFunSuiteLike with Matchers with LazyLogging {

  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  test("Very simple to create") {
    val future: Future[Int] = Future {
      1 + 1
    }

    future.map(_ shouldBe 2)
  }

  test("It is composable") {
    val futureA: Future[Int] = Future {
      Thread.sleep(200)
      println(s"future A: ${Thread.currentThread().getName}")
      1
    }

    val futureB = Future {
      println(s"future B: ${Thread.currentThread().getName}")
      2
    }

    val futureC = Future {
      Thread.sleep(300)
      println(s"future C: ${Thread.currentThread().getName}")
      3
    }

    val result: Future[Int] = for {
      a <- futureA
      b <- futureB
      c <- futureC
    } yield a + b + c

    result.map(_ shouldBe 6)
  }

  test("Memoization by default") {
    val future: Future[Int] = Future {
      println("Memoization")
      3
    }

    val result: Future[Int] = for {
      a <- future.map(_ + 1)
      b <- future.map(_ + 2)
      c <- future.map(_ + 3)
    } yield a + b + c

    result.map(_ shouldBe 15)
  }
}
