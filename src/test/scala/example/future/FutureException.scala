package example.future

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funsuite.AnyFunSuiteLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{ExecutionContext, Future}

class FutureException extends AnyFunSuiteLike with Matchers with ScalaFutures {

  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  test("Can memoize only on success?") {
    val future: Future[Int] = Future {
      println("Memoization")
      throw new IllegalArgumentException
    }

    whenReady(future.failed) { ex =>
      ex shouldBe a[IllegalArgumentException]
    }

    whenReady(future.failed) { ex =>
      ex shouldBe a[IllegalArgumentException]
    }
  }

}
