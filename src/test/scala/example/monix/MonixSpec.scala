package example.monix

import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import monix.execution.ExecutionModel.{AlwaysAsyncExecution, BatchedExecution, SynchronousExecution}
import monix.execution.schedulers.SchedulerService
import monix.execution.{CancelableFuture, Scheduler}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funsuite.AsyncFunSuiteLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Random

class MonixSpec extends AsyncFunSuiteLike with Matchers with ScalaFutures with LazyLogging {

  test("Very simple to create") {

    implicit val sc: Scheduler = Scheduler.io()

    val task: Task[Int] = Task {
      1 + 1
    }

    task.runToFuture.map(_ shouldBe 2)
  }

  test("It is composable too!") {

    implicit val sc: Scheduler = Scheduler.io()

    println(Thread.currentThread().getName)

    val taskA: Task[Int] = Task {
      println(s"task A: ${Thread.currentThread().getName}")
      1
    }.delayExecution(1.milli)

    val taskB = Task.now {
      println(s"task B: ${Thread.currentThread().getName}")
      2
    }

    val taskC = Task {
      println(s"task C: ${Thread.currentThread().getName}")
      3
    }.delayExecution(3.milli)

    val result: Task[Int] = for {
      a <- taskA
      b <- taskB
      c <- taskC
    } yield a + b + c

    result.runToFuture.map(_ shouldBe 6)
  }

  test("Memoization is optional") {

    implicit val sc: Scheduler = Scheduler.io()

    val task: Task[Int] = Task {
      println("No Memoization")
      3
    }

    val result: Task[Int] = for {
      a <- task.map(_ + 1)
      b <- task.map(_ + 2)
      c <- task.map(_ + 3)
    } yield a + b + c

    result.runToFuture.map(_ shouldBe 15)
  }

  test("memoize only on success") {

    implicit val sc: Scheduler = Scheduler.io()

    def increment(value: Int): Task[Int] = Task {
      println(s"Value $value is going to be incremented")
      if (value < 3) {
        println(s"value $value is incremented by 1")
        value + 1
      } else throw new RuntimeException("Value is greater that 2")
    }

    val result: Task[Int] = increment(1).memoizeOnSuccess

    val resultError = increment(3)
      .delayExecution(100.milli)
      .memoizeOnSuccess

    whenReady(resultError.runToFuture.failed) { ex =>
      ex shouldBe an[RuntimeException]
    }

    whenReady(resultError.runToFuture.failed) { ex =>
      ex shouldBe an[RuntimeException]
    }

    result.runToFuture.map(_ shouldBe 2)
    result.runToFuture.map(_ shouldBe 2)
  }

  test("Task can be canceled") {

    implicit val sc: Scheduler = Scheduler.computation(
      parallelism = 2,
      executionModel = AlwaysAsyncExecution
    )

    println(s"Main thread ==> ${Thread.currentThread().getName}")

    val task1: CancelableFuture[Int] = Task {
      println(s" Task 1 => ${Thread.currentThread().getName}")
      Thread.sleep(100)
      println("Task 1: Computation done")
      1
    }
      .memoizeOnSuccess
      .runToFuture

    val task2: CancelableFuture[Int] = Task {
      println(s" Task 2 => ${Thread.currentThread().getName}")
      Thread.sleep(1000)
      println("Task 2: Computation done")
      2
    }
      .memoizeOnSuccess
      .runToFuture

    val task3: CancelableFuture[Int] = Task {
      println(s" Task 3 => ${Thread.currentThread().getName}")
      Thread.sleep(5000)
      println("Task 3: Computation done")
      3
    }
      .memoizeOnSuccess
      .runToFuture

    Thread.sleep(1500)

    println("Task 3 is taking to long. cancelled")

    task3.cancel()

    Task.now(assert(true)).runToFuture
  }

  test("You can wrap a future execution into task") {
    implicit val sc: Scheduler = Scheduler.io()

    val future: Future[Int] = Future {
      println("I'm a future")
      1 + 1
    }

    val sum: Task[Int] = Task.fromFuture(future).map { x =>
      println("I'm a task")
      x + 1
    }.delayExecution(500.milli)

    sum.map(_ shouldBe 3).runToFuture
  }

//  test("wrap failed future") {
//
//    implicit val sc: Scheduler = Scheduler.io()
//
//    val future: Future[Int] = Future {
//      throw new RuntimeException("Future failed")
//    }
//
//    val sum: Task[Int] = Task.fromFuture(future)
//      .map { _ + 1 }
//
//    sum.onErrorRecoverWith {
//      case e => Task(assert(true))
//    }.runToFuture
//  }

  test("Defer") {
    implicit val sc: Scheduler = Scheduler.io()

    val sum = Task.deferFuture {
      println("I'm a task")
      Future {
        println("I'm a future")
        1 + 1
      }
    }.delayExecution(500.milli)

    sum.map(_ shouldBe 2).runToFuture
  }

  test("You can retry on error") {

    implicit val sc: SchedulerService = Scheduler.io()

    val source = Task(Random.nextInt()).flatMap {
      case even if even % 2 == 0 =>
        Task.now(even)
      case other =>
        println(s"Odd: $other")
        Task.raiseError(new IllegalStateException(other.toString))
    }

    val randomEven = source.onErrorRestart(maxRetries = 4)

    randomEven.map(_ % 2 shouldBe 0).runToFuture
  }
}
