import sbt._

object Versions {
  lazy val monixVersion: String = "3.4.0"
  lazy val scalatestVersion: String = "3.2.9"
  lazy val loggingVersion: String = "3.9.4"
}

object Dependencies {

  lazy val common: Seq[ModuleID] = Seq(
    "io.monix" %% "monix" % Versions.monixVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % Versions.loggingVersion
  )

  lazy val test: Seq[ModuleID] = Seq(
    "org.scalatest" %% "scalatest" % Versions.scalatestVersion % Test
  )
}
